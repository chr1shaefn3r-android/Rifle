package com.alien.rifle;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RifleImageView extends ImageView {
/*****************************************************************************/
/*  Variablen und Konstanten deklarieren                                     */
/*****************************************************************************/
private Paint ShotCount = new Paint();
private Bitmap ModeSingleshot = null;
private Bitmap ModeBurst = null;
private Bitmap ModeGrenade = null;
private int shots = 0;
private int WeaponMode = 0;
private int MaxShots = 30;
private float scale = 0;
private float modeleft = 130;
private float modetop = 2;
private float shotspositionx = 83;
private float shotspositiony = 18;

	public RifleImageView(Context context) {
		super(context);
		initialize(context);
	}
	
	public RifleImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(context);
	}

	public RifleImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
		initialize(context);
	}
	
	private void initialize(Context context) {
		ShotCount.setARGB(255, 150, 205, 129);
		ShotCount.setTextSize(20);
		ModeBurst = BitmapFactory.decodeResource(getResources(), R.drawable.burst);
		ModeSingleshot = BitmapFactory.decodeResource(getResources(), R.drawable.single);
		ModeGrenade = BitmapFactory.decodeResource(getResources(), R.drawable.grenade);
		scale = getContext().getResources().getDisplayMetrics().density;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		int shot = MaxShots-shots;
		if(shot > (MaxShots*0.25)) {
			// Solange die verbleibenden Schusszahl gr��er als 25% der Gesamtschusszahl ist wird die verbleibende Schusszahl im Crysisgr�n ausgegeben
			ShotCount.setARGB(255, 150, 205, 129);
		} else if(shot <= (MaxShots*0.25) && shot > 0){
			// Solange die verbleibenden Schusszahl kleiner als 25% der Gesamtschusszahl, aber gr��er 0 ist wird die verbleibende Schusszahl in einem Rotstich ausgegeben
			ShotCount.setARGB(200, 197, 31, 31);
		} else {
			// Wenn die verbleibende Schusszahl auf 0 gefallen ist wird sie Rot ausgegeben
			ShotCount.setARGB(255, 255, 0, 0);
		}
		canvas.drawText(""+shot, shotspositionx*scale, shotspositiony*scale, ShotCount);
		switch(WeaponMode) {
			case 0:
				canvas.drawBitmap(ModeSingleshot, scale*modeleft, scale*modetop, null);
				break;
			case 1:
				canvas.drawBitmap(ModeBurst, scale*modeleft, scale*modetop, null);
				break;
			case 2:
				canvas.drawBitmap(ModeGrenade,scale*modeleft, scale*modetop, null);
				break;
		}
	}

	/*************************************************************************/
	/*  Getters und Setters                                                  */
	/*************************************************************************/
	public void setMaxshots(int maxshots) {
		this.MaxShots = maxshots;
	}
	// shots
	public void setShots(int shots) {
		this.shots = shots;
	}
	public void setWeaponMode(int weaponMode) {
		this.WeaponMode = weaponMode;
	}
	
}
