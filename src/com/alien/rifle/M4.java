package com.alien.rifle;

import android.media.MediaPlayer;
import android.util.Log;

public class M4 extends Rifles {

	public M4(Rifle rifle) {
		dieRifle = rifle;
		shots = 0;
		weaponMode = 0;
		maxShots = 30;
		resIDSingleShot = R.raw.m4a1_einzelschuss;
		resIDSingleShotSilencer = R.raw.m4a1_einzelschuss_schalldaempfer;
		resIDBurstMode = R.raw.m4a1_dauerfeuer;
		resIDBurstModeSilencer = R.raw.m4a1_dauerfeuer_schalldaempfer;
		resIDGrenadeLauncher = R.raw.grenade_launcher;
		
		silencerOn = MediaPlayer.create(dieRifle, R.raw.m4a1_silencer_on);
        silencerOff = MediaPlayer.create(dieRifle, R.raw.m4a1_silencer_off);
        reload = MediaPlayer.create(dieRifle, R.raw.m4a1_nachladen);
        changemode = MediaPlayer.create(dieRifle, R.raw.change_mode);
	}

	@Override
	void nextMode() {
		changemode.start();
		switch(weaponMode) {
			case 0:
				weaponMode = 1; // Einzelschuss => Dauerfeuer
				break;
			case 1:
				weaponMode = 2; // Dauerfeuer => Granatwerfer
				break;
			case 2:
				weaponMode = 0; // Granatwerfer => Einzelschuss
				break;
			default:
				weaponMode = 0; // Standard: Einzelschuss
				break;
		}
	}

	@Override
	void reloadWeapon(boolean sound) {
		if(sound) reload.start();
		shots = 0;
	}

	@Override
	void toggleSilencer() {
		if(silencerState) {
    		silencerOff.start();
    		silencerState = false;
        } else {
        	silencerOn.start();
        	silencerState = true;
        }
	}

	@Override
	public int getSound() {
		Log.d("M4", "WeaponMode: "+weaponMode+" / Schalldämpfer: "+silencerState);
		switch(weaponMode) {
			case 0: //Einzelschuss
				if(silencerState) {
					Log.d("M4", "m4a1_einzelschuss_schalldaempfer");
					return R.raw.m4a1_einzelschuss_schalldaempfer;
				} else {
					Log.d("M4", "m4a1_einzelschuss");
					return R.raw.m4a1_einzelschuss;
				}
		case 1: //Dauerfeuer
				if(silencerState) {
					Log.d("M4", "m4a1_dauerfeuer_schalldaempfer");
					return R.raw.m4a1_dauerfeuer_schalldaempfer;
				} else {
					Log.d("M4", "m4a1_dauerfeuer");
					return R.raw.m4a1_dauerfeuer;
				}
		case 2:	//Granatwerfer
				Log.d("M4", "grenade_launcher");
				return R.raw.grenade_launcher;
		}
		return 0;
	}

}
