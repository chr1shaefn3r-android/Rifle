package com.alien.rifle;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.Button;

public class CrysisButton extends Button {

	public CrysisButton(Context context) {
		super(context);
		initialize(context);
	}
	
	public CrysisButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize(context);
	}

	public CrysisButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs);
		initialize(context);
	}
	
	protected void initialize(Context context) {
		// Als Hintergrundbild die XML-Datei res/drawable/states.xml gesetzt
		// in der wird auch gleich definiert in welchem Zustand welches Hintergrundbild gezeigt wird
		this.setBackgroundResource(R.drawable.states);
		
		// Als Textfarbe die XML-Datei res/color/states.xml gesetzt
		// in der wird auch gleich definiert in welchem Zustand welches Textfarbe genutzt wird
		this.setTextColor(context.getResources().getColorStateList(R.color.states));
		
		// Textgr��e auf 12pt setzen
		this.setTextSize(TypedValue.COMPLEX_UNIT_PT, 12);
		
		// Text links ausrichten
		this.setGravity(Gravity.LEFT);
		
		// Padding links von 12px und oben von 4px setzen
		this.setPadding(12, 4, 0, 0);
		
		// Den Button focusierbar machen
		this.setFocusable(true);
	}
}
