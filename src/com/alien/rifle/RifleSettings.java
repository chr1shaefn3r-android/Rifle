package com.alien.rifle;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class RifleSettings extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		RifleSettings.this.finish();
	}
	
	/*************************************************************************/
	/* Menu-Methoden                                                         */
	/*************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.settings, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
        case R.id.save:
        	Toast.makeText(this, RifleSettings.this.getString(R.string.settings_saved), Toast.LENGTH_SHORT).show();
        	break;
        case R.id.back:
        	RifleSettings.this.finish();
            break;
		}
		return super.onOptionsItemSelected(item);
	}
}
