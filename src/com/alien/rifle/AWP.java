package com.alien.rifle;

import android.media.MediaPlayer;

public class AWP extends Rifles {

	public AWP(Rifle rifle) {
		dieRifle = rifle;
		shots = 0;
		weaponMode = 0;
		maxShots = 10;
		resIDSingleShot = R.raw.awp_einzelschuss;
		
		reload = MediaPlayer.create(dieRifle, R.raw.awp_nachladen);
        changemode = MediaPlayer.create(dieRifle, R.raw.change_mode);
	}

	@Override
	void nextMode() {
		changemode.start();
		switch(weaponMode) {
			default:
				weaponMode = 0; // Standard: Einzelschuss
				break;
		}
	}

	@Override
	void reloadWeapon(boolean sound) {
		if(sound) reload.start();
		shots = 0;
	}

	@Override
	void toggleSilencer() {
    	silencerState = false;
	}

	@Override
	public int getSound() {
		switch(weaponMode) {
			default: //Einzelschuss
					return R.raw.awp_einzelschuss;
		}
	}

}
