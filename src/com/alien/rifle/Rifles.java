package com.alien.rifle;

import android.app.Activity;
import android.media.MediaPlayer;

abstract public class Rifles extends Activity {
	/*****************************************************************************/
	/*  Variablen und Konstanten deklarieren                                     */
	/*****************************************************************************/
	protected int shots = 0;
	protected int weaponMode = 0;
	protected int maxShots = 0;
	protected int sound = 0;
	protected int resIDReload = 0;
	protected int resIDSilencerOn = 0;
	protected int resIDSilencerOff = 0;
	protected int resIDSingleShot = 0;
	protected int resIDSingleShotSilencer = 0;
	protected int resIDBurstMode = 0;
	protected int resIDBurstModeSilencer = 0;
	protected int resIDGrenadeLauncher = 0;
	protected int resIDDryFire = R.raw.dryfire_rifle;
	protected boolean silencerState = false;
	
	protected Rifle dieRifle;
	
	protected MediaPlayer silencerOn;
	protected MediaPlayer silencerOff;
	protected MediaPlayer reload;
	protected MediaPlayer changemode;
	
	abstract void nextMode();
	
	abstract void toggleSilencer();
	
	abstract void reloadWeapon(boolean sound);
	
	public abstract int getSound();
	
	public int getMaxShots() {
		return maxShots;
	}
	public void setMaxShots(int maxshots) {
		this.maxShots = maxshots;
	}
	
	public int getShots() {
		return shots;
	}
	public void setShots(int shots) {
		this.shots = shots;
	}

	public boolean getSilencerState() {
		return silencerState;
	}

	public int getWeaponMode() {
		return weaponMode;
	}
	
	public int getResIDSingleShot() {
		return resIDSingleShot;
	}

	public int getResIDSingleShotSilencer() {
		return resIDSingleShotSilencer;
	}

	public int getResIDBurstMode() {
		return resIDBurstMode;
	}

	public int getResIDBurstModeSilencer() {
		return resIDBurstModeSilencer;
	}

	public int getResIDGrenadeLauncher() {
		return resIDGrenadeLauncher;
	}
}
