package com.alien.rifle;

import java.io.IOException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Rifle extends Activity {
	
/*****************************************************************************/
/*  Variablen und Konstanten deklarieren                                     */
/*****************************************************************************/
// Konstante
private static final int STREAM_MUSIC = 3;
private static final int SETTINGS = 42;
// Variablen
private Button ButtonPower;
private Button ButtonReload;
private Button ButtonSilencer;
private Button ButtonMode;
private RifleImageView state;
private MediaPlayer mp;
private PowerManager.WakeLock wl;
private AudioManager am;
private Vibrator vibrator = null;
private Handler handler;
private int volume = 0;
private boolean first = true;				//Damit bei leerem Magazin, der dryfire-Sound nur beim ersten mal erzeugt wird
private boolean firstHandler = true;		//Damit der Handler im Burstmodus nur beim ersten Durchlauf eingerichtet wird
private boolean FireButtonIsPressed = true;	//Um richtiges Einzelschuss zu realisieren
private boolean Vibration = false;			//Ob ein Rückstoß via Vibration simuliert werden soll
private SharedPreferences settings;
private Rifles weapon;
	
	/*************************************************************************/
	/*  onCreate Method - Wird beim Start aufgerufen                         */
	/*************************************************************************/
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Log.d("Rifle", "Got an onCreate()");
        
        /*********************************************************************/
    	/*  PowerManager und WakeLock initialisieren und anwenden            */
    	/*********************************************************************/
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My WakeLock");
        wl.acquire();
        
        /*********************************************************************/
    	/*  Handler initialisieren                                           */
    	/*********************************************************************/
        handler = new Handler();
        
        /*********************************************************************/
    	/*  Vibrationsservice initalisieren                                  */
    	/*********************************************************************/
        vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
        
        /*********************************************************************/
    	/*  Standardeinstellungswerte aus settings.xml laden                 */
    	/*********************************************************************/
        PreferenceManager.setDefaultValues(this, R.xml.settings, false);

        /*********************************************************************/
    	/*  Saemtliche GUI-Elemente initialisieren und verfuegbar machen     */
    	/*********************************************************************/
        ButtonPower = (Button)findViewById(R.id.ButtonPower);
        ButtonReload = (Button)findViewById(R.id.ButtonReload);
        ButtonSilencer = (Button)findViewById(R.id.ButtonSilencer);
        ButtonMode = (Button)findViewById(R.id.ButtonMode);
        state = (RifleImageView)findViewById(R.id.crysis_state);
        
        /*********************************************************************/
    	/*  Einstellungen initialisieren                                     */
    	/*********************************************************************/
        settings = PreferenceManager.getDefaultSharedPreferences(Rifle.this);
        
        /*********************************************************************/
    	/*  Ausw�hlen welches Gewehr erzeugt werden soll                     */
    	/*********************************************************************/
        if(settings.getString("WeaponType", null).contentEquals("m4")) {
       	Log.d("M4", "WeaponType equals m4");
        	weapon = new M4(Rifle.this);
        } else if (settings.getString("WeaponType", null).contentEquals("awp")) {
        	Log.d("M4", "WeaponType equals awp");
        	weapon = new AWP(Rifle.this);
        } else {
        	weapon = new M4(Rifle.this);
        }
        Rifle.this.getSettings();
        	
        /*********************************************************************/
    	/*  Den eigentlichen Schusssound initialisieren                      */
    	/*********************************************************************/
        mp = MediaPlayer.create(Rifle.this, R.raw.dryfire_rifle);
        
        /*********************************************************************/
    	/*  AudioManager initialisieren, um Lautstaerke aendern zu koennen   */
    	/*********************************************************************/
        am = (AudioManager) this.getSystemService(AUDIO_SERVICE);
        volume = am.getStreamVolume(STREAM_MUSIC); // 3 = constant STREAM_MUSIC
        am.setStreamVolume(STREAM_MUSIC, am.getStreamMaxVolume(STREAM_MUSIC), RESULT_OK);
        
        /*********************************************************************/
    	/*  onClickListener fuer die Nachladen-Taste                         */
    	/*********************************************************************/
        ButtonReload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Rifle.this.reload(true);
            }
        });
        
        /*********************************************************************/
    	/*  onClickListener fuer die Schalldaempfer-Taste                    */
    	/*********************************************************************/
        ButtonSilencer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Rifle.this.silencer();
            }
        });
        
        /*********************************************************************/
    	/*  onClickListener fuer die Modus-Taste                             */
    	/*********************************************************************/
        ButtonMode.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Rifle.this.mode();
            }
        });
        
        /*********************************************************************/
    	/*  onClickListener fuer die Shutdown-Taste                          */
    	/*********************************************************************/
        ButtonPower.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Rifle.this.onPause();
                System.exit(0);
            }
        });
    }

    @Override
	protected void onStart() {
		super.onStart();
		Log.d("Rifle", "Got an onStart()");
	}

	/*************************************************************************/
	/*  updateShotsTask - Task den der Handler alle 150ms ausfuehrt          */
    /*************************************************************************/
	private Runnable updateShotsTask = new Runnable() {
    	public void run() {
    		weapon.setShots(weapon.getShots()+1);
    		state.setShots(weapon.getShots());
    		state.invalidate();
    		if(Vibration) vibrator.vibrate(100);
    		handler.postDelayed(this, 150);
    	}
    };
    
    /*************************************************************************/
	/*  onKeyDown - faengt die Tastendruecke nach unten ab                   */
	/*************************************************************************/
	@Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
        	if(weapon.getShots() < weapon.getMaxShots()) {
        		first = true;
            	if(weapon.getWeaponMode() == 0) {
            		/*********************************************************/
            		/*  Singleshot - Modus                                   */
            		/*********************************************************/
            		//Singleshot
            		if(FireButtonIsPressed) {
            			try {
    						Thread.sleep(50);
    					} catch (InterruptedException e1) {
    						e1.printStackTrace();
    					}
            			if(!mp.isPlaying()) {
                			mp.start();
                			weapon.setShots(weapon.getShots()+1);
                			state.setShots(weapon.getShots());
                			state.invalidate();
                			if(Vibration) vibrator.vibrate(100);
            			}
            			FireButtonIsPressed = false;
            		}
            	} else if(weapon.getWeaponMode() == 1) {
            		/*********************************************************/
            		/*  Burst - Modus                                        */
            		/*********************************************************/
            		if(firstHandler) {
            			if(Vibration) vibrator.vibrate(100);
                    	handler.postDelayed(updateShotsTask, 150);
                    	firstHandler=false;
                    	mp.start();
            		}
            	} else {
            		/*********************************************************/
            		/*  Grenade Launcher - Modus                             */
            		/*********************************************************/
            		if(FireButtonIsPressed) {
            			try {
    						Thread.sleep(50);
    					} catch (InterruptedException e1) {
    						e1.printStackTrace();
    					}
            			if(!mp.isPlaying()) {
                			mp.start();
                			weapon.setShots(weapon.getShots()+1);
                			state.setShots(weapon.getShots());
                			state.invalidate();
            			}
            			FireButtonIsPressed = false;
            		}
            	}
        	} else {
        		firstHandler=true;
        		state.invalidate();
        		if(first) {
        			mp.stop();
        			mp.release();
        			mp = MediaPlayer.create(this, R.raw.dryfire_rifle);
        			handler.removeCallbacks(updateShotsTask);
        			first = false;
        		}
        		try {
					Thread.sleep(50);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
    			if(!mp.isPlaying()) {
        			mp.start();
    			}
        	}

        }
        else if(keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS) {
        	Rifle.this.silencer(); // "link" Kabelheadsettaste
        } else if(keyCode == KeyEvent.KEYCODE_HEADSETHOOK) {
        	Rifle.this.mode(); // mittlere Kabelheadsettaste
        } else if(keyCode == KeyEvent.KEYCODE_MEDIA_NEXT) {
        	Rifle.this.reload(true); // "rechte" Kabelheadsettaste
        } else if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_DPAD_DOWN ||
        		  keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT ||
        		  keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_MENU) {
        	// Dpad/Trackball soll vom System vearbeitet werden!
        	// und auch der Men�-Button soll vom System verarbeitet werden!
        	return false;
        }
        return true;
    }
    
	/*************************************************************************/
	/*  onKeyUp - faengt das Tastenloslassen ab                              */
	/*************************************************************************/
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
        	FireButtonIsPressed = true;
        	if(weapon.getShots() < weapon.getMaxShots()) {
	    		if(weapon.getWeaponMode() == 0) {
	        		// Singleshot - Modus
	    		} else if(weapon.getWeaponMode() == 1){
	        		// Burst - Modus
	    			handler.removeCallbacks(updateShotsTask);
	    			firstHandler = true;
					mp.stop();
					try {
						Thread.sleep(50);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
					try {
						mp.prepare();
					} catch (IllegalStateException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
	    		} else {
	        		// Grenade Launcher - Modus
	    		}
        	} else {
        		// do nothing...
        	}
        } else if(keyCode == KeyEvent.KEYCODE_DPAD_CENTER || keyCode == KeyEvent.KEYCODE_DPAD_DOWN ||
      		  keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_DPAD_RIGHT ||
      		  keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_MENU) {
      	// Dpad/Trackball soll vom System vearbeitet werden!
      	// und auch der Men�-Button soll vom System verarbeitet werden!
      	return false;
      }
        return true;
    }
    
    /*************************************************************************/
	/* mode()                                                                */
	/* Ruft die nextMode-Methode der Waffe auf, fr�gt den daraus resultieren */
    /* den neuen Modus ab und zeigt den entsprechenden Text auf dem Button an*/
	/*************************************************************************/
    private void mode() {
    	weapon.nextMode();
    	switch(weapon.getWeaponMode()) {
    		case 0:
    			ButtonMode.setText(Rifle.this.getString(R.string.singleshot));
    			break;
    		case 1:
    			ButtonMode.setText(Rifle.this.getString(R.string.burstmode));
    			break;
    		case 2:
    			ButtonMode.setText(Rifle.this.getString(R.string.grenadelauncher));
    			break;
    		default:
    			ButtonMode.setText(Rifle.this.getString(R.string.singleshot));
    			break;
    	}
    	state.setWeaponMode(weapon.getWeaponMode());
    	state.invalidate();
    	Rifle.this.setSound();
    }
    
    /*************************************************************************/
	/* reload()                                                              */
	/* Spielt den Nachladensound ab und setzt die Schussanzahl auf 0 zur�ck  */
	/*************************************************************************/
    private void reload(boolean sound) {
    	Log.d("M4", "Waffentyp: "+settings.getString("WeaponType", null));
    	weapon.reloadWeapon(sound);
    	state.setShots(weapon.getShots());
    	handler.removeCallbacks(updateShotsTask);
        state.invalidate();
        Rifle.this.setSound();
    }
    
    /*************************************************************************/
	/* silencer()                                                            */
	/* Ruft die toggleSilencer-Methode der Waffe auf und zeigt den           */
    /* entsprechenden Text auf dem Button an                                 */
	/*************************************************************************/
    private void silencer() {
    	weapon.toggleSilencer();
    	if(weapon.getSilencerState()) {
        	ButtonSilencer.setText(Rifle.this.getString(R.string.silenceroff));
        } else {
        	ButtonSilencer.setText(Rifle.this.getString(R.string.silenceron));
        }
        Rifle.this.setSound();
    }
    
    /*************************************************************************/
	/* setSound()                                                            */
	/* Um in Abhaengigkeit von den aktuell gesetzen ToogleButtons das Media- */
    /* Playerobjekt (mp) richtig zu setzen                                   */
	/*************************************************************************/
    private void setSound() {
    	if(weapon.getShots() < weapon.getMaxShots()) {
	    	// MediaPlayerressourcen frei lassen
	    		mp.release();
	    		mp = MediaPlayer.create(Rifle.this, weapon.getSound());
	    		if(weapon.getWeaponMode() != 1) {
	    			mp.setLooping(false);
	    		} else {
	    			mp.setLooping(true);
	    		}
	    	}
    }
    
    /*************************************************************************/
	/* getSettings()                                                         */
	/* Liest Settings aus den SharedPreferences aus und speichert sie in     */
    /* lokale Variablen ab                                                   */
	/*************************************************************************/
    private void getSettings() {
    	weapon.setMaxShots(new Integer(settings.getString("MaxShots", null)));
    	state.setMaxshots(weapon.getMaxShots());
    	Vibration = settings.getBoolean("Recoil", false);
		state.invalidate();
    }
    
	/*************************************************************************/
	/* Menu-Methoden                                                         */
	/*************************************************************************/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	    MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	}
    
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
            	Intent myIntent = new Intent(Rifle.this, RifleSettings.class);
            	Rifle.this.startActivityForResult(myIntent, SETTINGS);
            	break;
            case R.id.quit:
				finish();
                break;
        }
        return true;
    }
	
	/*************************************************************************/
	/* Ergebnise der gestarteten Chield-Activitys auswerten                  */
	/*************************************************************************/
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		switch(requestCode) {
		case SETTINGS:
			/* Settings-Activity (com.alien.rifle.RifleSettings) */
			Toast.makeText(this, Rifle.this.getString(R.string.settings_saved), Toast.LENGTH_SHORT).show();
			if(settings.getString("WeaponType", null).contentEquals("m4")) {
	        	Log.d("M4", "WeaponType equals m4");
	        	weapon = null;
	        	weapon = new M4(Rifle.this);
	        } else if (settings.getString("WeaponType", null).contentEquals("awp")) {
	        	Log.d("M4", "WeaponType equals awp");
	        	weapon = null;
	        	weapon = new AWP(Rifle.this);
	        } else {
	        	weapon = null;
	        	weapon = new M4(Rifle.this);
	        }
			Rifle.this.reload(false); // Nachladen ohne Sound!
			Rifle.this.mode();
			Rifle.this.getSettings();
			Rifle.this.setSound();
			break;
		}
	}
	
    /*************************************************************************/
	/* die Restlichen on* Methods                                            */
	/*************************************************************************/
    @Override
	protected void onResume() {
		super.onResume();
		Log.d("Rifle", "Got an onResume()");
		if(!wl.isHeld()) {
			wl.acquire();
		}
		handler.removeCallbacks(updateShotsTask);
		am.setStreamVolume(STREAM_MUSIC, am.getStreamMaxVolume(STREAM_MUSIC), RESULT_OK);
		Rifle.this.setSound();
	}
    
	@Override
	protected void onPause() {
		super.onPause();
		Log.d("Rifle", "Got an onPause()");
		mp.stop();
		mp.release();
		am.setStreamVolume(STREAM_MUSIC, volume, RESULT_OK);
		wl.release();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		Log.d("Rifle", "Got an onStop()");
		handler.removeCallbacks(updateShotsTask);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.d("Rifle", "Got an onDestroy()");
		if(handler != null) {
			handler.removeCallbacks(updateShotsTask);
		}
	}
}
